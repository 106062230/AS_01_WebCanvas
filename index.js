window.onload = DrawTools;

var canvas;
var ctx;
var sizeofpen;
var color;
var image_save = [];
var save;
var idx = 0;
var tools = 'pencil';
var font_size = "12px";
var font_kind = "PMingLiU";
var temp = '12px PMingLiU'
var point = {
    x:0,
    y:0
};

function DrawTools()
{
    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');
    ctx.lineJoin = "round";
    ctx.lineCap = "round";
    ctx.font = temp;
    ctx.globalCompositeOperation = 'source-over';
    sizeofpen = document.getElementById('draw_size');
    color = document.getElementById('myColor');
    image_save.push(document.getElementById('myCanvas').toDataURL());

    var draw_or_not = false;
    var lastX;
    var lastY;

    function draw(e){
        if(!draw_or_not) {
           return; 
        }
        if(tools == 'pencil' || tools == 'eraser'){
            ctx.beginPath();
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.closePath();
            ctx.stroke();
            lastX = e.offsetX;
            lastY = e.offsetY;
        }
        else if(tools == 'rectangle'){
            var img = new Image();
            img.src = save;
            img.onload = () => {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0); 
                ctx.beginPath();
                if(point.x < e.offsetX && point.y < e.offsetY){
                    ctx.rect(point.x, point.y, e.offsetX - point.x, e.offsetY - point.y);
                }
                else if(point.x < e.offsetX && point.y >= e.offsetY){
                    ctx.rect(point.x, e.offsetY, e.offsetX - point.x, point.y - e.offsetY);
                }
                else if(point.x >= e.offsetX && point.y < e.offsetY){
                    ctx.rect(e.offsetX, point.y, point.x - e.offsetX, e.offsetY - point.y);
                }
                else{
                    ctx.rect(e.offsetX, e.offsetY, point.x - e.offsetX, point.y - e.offsetY);
                }
                ctx.stroke();
            }
        }
        else if(tools == 'circle'){
            var img = new Image();
            var centerX = (point.x + e.offsetX)/2;
            var centerY = (point.y + e.offsetY)/2;
            var r = (Math.abs(point.x - e.offsetX) > Math.abs(point.y - e.offsetY))? (Math.abs(point.y - e.offsetY))/2 : (Math.abs(point.x - e.offsetX))/2;
            img.src = save;
            img.onload = () => {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0); 
                ctx.beginPath();
                ctx.arc(centerX, centerY, r, 0, 2 * Math.PI, false);
                ctx.stroke();
            }
        }
        else if(tools == 'triangle'){
            var img = new Image();
            img.src = save;
            img.onload = () => {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(img, 0, 0); 
                ctx.beginPath();
                ctx.moveTo((point.x + e.offsetX)/2, point.y);
                ctx.lineTo(point.x, e.offsetY);
                ctx.lineTo(e.offsetX, e.offsetY);
                ctx.lineTo((point.x + e.offsetX)/2, point.y);
                ctx.stroke();
            }
        }
    }
    function notDraw(){
        draw_or_not = false;
    }
    function toDraw(e){
        draw_or_not = true;
        lastX = e.offsetX;
        lastY = e.offsetY;
        point.x = e.offsetX;
        point.y = e.offsetY;
        save = document.getElementById('myCanvas').toDataURL();
    }
    sizeofpen.addEventListener("change",() => {
        ctx.lineWidth = document.getElementById('draw_size').value;
    });
    color.addEventListener("change",() => {
        ctx.strokeStyle = document.getElementById('myColor').value;
        ctx.fillStyle = document.getElementById('myColor').value;
    });
    
    canvas.addEventListener('mousedown', toDraw);
    canvas.addEventListener('mouseup', () => {
        draw_or_not = false;
        if(tools != 'text' && tools != 'redo' && tools != 'undo' && tools != 'color' && tools != 'upload' && tools != 'download'){
            idx++;
            console.log(idx);
            if (idx < image_save.length) { image_save.length = idx; }
            image_save.push(document.getElementById('myCanvas').toDataURL());
        }
    });
    canvas.addEventListener('mouseout', notDraw);
    canvas.addEventListener('mousemove', draw);
    
}

function doRedoUndo(num)
{   
    idx = idx + num;
    var Pic = new Image();
    Pic.src = image_save[idx];
    Pic.onload = function () { 
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(Pic, 0, 0); 
    }
}

function dotext(){
    var empty = true;

    canvas.onclick = function(e) {
        if (!empty) return;
        var input = document.createElement('input');
        input.type = 'text';
        input.onkeydown = handleEnter;
        document.body.appendChild(input);
        input.focus();
        empty = false;
    }

    function handleEnter(e) {
        var keyCode = e.keyCode;
        if (keyCode === 13) {
            //ctx.font = Text;
            //ctx.fillStyle = last_color;
            ctx.textBaseline = 'top';
            console.log(this.value);
            ctx.fillText(this.value, point.x, point.y);
            document.body.removeChild(this);
            empty = true;
            idx++;
            console.log(idx);
            if (idx < image_save.length) { image_save.length = idx; }
            image_save.push(document.getElementById('myCanvas').toDataURL());
        }
    }

}

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}

function change(tool){
    if(tools == 'upload'){
        idx++;
        console.log(idx);
        if (idx < image_save.length) { image_save.length = idx; }
        image_save.push(document.getElementById('myCanvas').toDataURL());
    }
    tools = tool;
    canvas.onclick = null;
    canvas.style.cursor = "url('other.jpg'), default";
    if(tool == 'pencil'){
        ctx.globalCompositeOperation = 'source-over';
        canvas.style.cursor = "url('pencil.png'), default";
    }
    else if(tool == 'eraser'){
        ctx.globalCompositeOperation = 'destination-out';
        canvas.style.cursor = "url('eraser.png'), default";
    }
    else if(tool == 'text'){
        ctx.globalCompositeOperation = 'source-over';
        canvas.style.cursor = "url('text.png'), default";
        dotext();
    }
    else if(tool == 'undo'){
        ctx.globalCompositeOperation = 'source-over';
        if(idx>0) doRedoUndo(-1);
    }
    else if(tool == 'redo'){
        ctx.globalCompositeOperation = 'source-over';
        if(idx<image_save.length-1) doRedoUndo(1);
    }
    else if(tool == 'rectangle'){
        ctx.globalCompositeOperation = 'source-over';
    }
    else if(tool == 'circle'){
        ctx.globalCompositeOperation = 'source-over';
    }
    else if(tool == 'upload'){
        ctx.globalCompositeOperation = 'source-over';
        var picture = document.getElementById('picture');
        picture.addEventListener('change', (e) => {
            var reader = new FileReader();
            reader.onload = function(event){
                var img = new Image();
                img.onload = function(){
                    //canvas.width = img.width;
                    //canvas.height = img.height;
                    ctx.drawImage(img,0,0);
                }
                img.src = event.target.result;
            }
            reader.readAsDataURL(e.target.files[0]); 
        });
    }
}

function change_kind(txt){
    font_kind = txt;
    temp = font_size + " " + font_kind;
    ctx.font = temp;
    if(txt == 'PMingLiU'){
        document.getElementById("dropdownFont").innerHTML = "細明體";
    }
    else if(txt == 'DFKai-sb'){
        document.getElementById("dropdownFont").innerHTML = "標楷體";
    }
    else if(txt == 'Microsoft JhengHei'){
        document.getElementById("dropdownFont").innerHTML = "微軟正黑體";
    }
    else{
        document.getElementById("dropdownFont").innerHTML = txt;
    }
}

function change_size(txt){
    font_size = txt;
    temp = font_size + " " + font_kind;
    ctx.font = temp;
    document.getElementById("dropdownSize").innerHTML = txt[0] + txt[1];
}

function reset(){
    idx++;
    console.log(idx);
    if (idx < image_save.length) { image_save.length = idx; }
    image_save.push(document.getElementById('myCanvas').toDataURL());
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}







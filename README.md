# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

這次的作業是要使用 html5 中的 **canvas** 實作出一個網頁版的小畫家

<img src="page.jpg" width="850px" height="500px"></img>

* **功能介紹**

    * **畫筆**  <img src="pencil.jpg" width="40px" height="40px"></img>

        小畫家繪圖的基本工具，可靠它來隨意作畫。


    * **橡皮擦**  <img src="eraser.jpg" width="40px" height="40px"></img>

        可將canvas中不喜歡或是畫錯的地方擦除。


    * **文字**  <img src="word.jpg" width="40px" height="40px"></img>
    
        可在畫布上點選任意地方，在工具列的下方輸入文字後，就會在剛剛點選畫布的地方產生文字。


    * **顏色** <img src="color.jpg" width="40px" height="40px"></img>
    
        可依照自己的喜好選擇顏色，改變畫筆、文字的顏色。


    * **回到上一步** <img src="undo.jpg" width="40px" height="40px"></img>
    
        可以將畫布回復到上一步作畫的結果，不斷點選可一直還原至畫布空白為止。


    * **回到下一步** <img src="redo.jpg" width="40px" height="40px"></img>
    
        若按了太多**回到上一步**，可點選**回到下一步**以恢復畫布。


    * **上傳** <img src="upload.jpg" width="40px" height="40px"></img>
    
        點選後，可將圖片上傳至畫布。


    * **下載** <img src="download.png" width="40px" height="40px"></img>
    
        點選後，可將畫布下載到自己的電腦中。

    * **Reset** <img src="reset.jpg" width="40px" height="40px"></img>

        點選按鈕後，即可將畫布清空。

    * **圖形** <img src="circle.png" width="40px" height="40px"></img><img src="rectangle.png" width="40px" height="40px"></img><img src="triangle.png" width="40px" height="40px"></img>
    
        點選這三個按鈕後，可依照自己的喜好畫出不同大小的圖形。


    * **畫筆粗細**  <img src="painter.jpg" width="63px" height="40px"></img>
    
        可以透過拖拉底下的範圍，調整畫筆的粗細。


    * **文字大小及字體**  <img src="wordtype.jpg" width="60px" height="40px"></img>
    
        可以用來選取字體大小跟字型。


* **游標 & 整體功能大覽**
    
    <img src="1.gif"></img>

    游標有**畫筆**、**橡皮擦**、**文字**、**其他**，可透過點選左方工具列點選tool，移動到畫布的時候就會更改游標了。

    點選左邊工具列的話，就會觸及onclick的事件，所以會呼叫javascript裡的change來更改使用的tool，而change會更改tool後，會再根據tool的不同來做不一樣的分配及處理；至於canvas的部分，主要是透過addEventListener去監聽滑鼠的事件，mousedown的時候就代表開始繪圖、mousemove的時候代表正在繪圖、mouseup的時候則代表繪圖結束。

* **心得**
    
    透過這次作業，對javascript算是比較上手了，之前lab寫計算機的時候，還不太清楚要怎麼寫，寫完這次作業之後，學到了很多不同的東西，其中最深刻的，莫過於自己上網學習該如何去實作，畢竟老師不可能教授大家所有的辦法，希望透過這次作業，能讓自己奠定下基礎功夫，才不會讓之後的lab、作業、project壓垮自己。


